const fs = require('fs');

// 1. Read the given file lipsum.txt

const problem2 = (filePath) => {
    fs.readFile('./lipsum.txt', (error, data) => {
        if (error) {
            console.log(error);
        }
        else {
            const lipsumData = data.toString().toUpperCase();
            fs.writeFile('./newFile1.txt', lipsumData, (error) => {
                if (error) {
                    console.log(error);
                }
                else {
                    // console.log(lipsumData);
                    fs.appendFile('./filenames.txt', `newFile1.txt\n`, (error) => {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            //console.log("success")
                            fs.readFile('./newFile1.txt', (error, data) => {
                                if (error) {
                                    console.log(error);
                                }
                                else {
                                    //console.log(data.toString());
                                    const lowerCase = data.toString().toLowerCase().split('. ').join('.\n');//.toString();
                                    fs.writeFile('./newFile2.txt', lowerCase, (error) => {
                                        if (error) {
                                            console.log(error);
                                        }
                                        else {
                                            //console.log(lowerCase);
                                            fs.appendFile('./filenames.txt', `newFile2.txt\n`, (error) => {
                                                if (error) {
                                                    console.log(error);
                                                }
                                                else {
                                                    //console.log("Success");

                                                    //console.log(data.toString());
                                                    fs.readFile('./newFile2.txt', (error, data) => {
                                                        if (error) {
                                                            console.log(error);
                                                        }
                                                        else {
                                                            //data.s;
                                                            let sort = data.toString().split('\n').sort().slice(3);
                                                            fs.writeFile('./sortFile.txt', sort.join('\n').toString(), (error) => {
                                                                if (error) {
                                                                    console.log(error)
                                                                }
                                                                else {
                                                                    //console.log(sort);
                                                                    fs.appendFile('./filenames.txt', `sortFile.txt`.toString(), (error) => {
                                                                        if (error) {
                                                                            console.log(error);
                                                                        }
                                                                        else {
                                                                            //console.log("success");


                                                                        }
                                                                    });
                                                                    fs.readFile('filenames.txt', 'utf8', (error, data) => {
                                                                        if (error) {
                                                                            console.log(error);
                                                                        }
                                                                        else {
                                                                            const filenames = data.split('\n');
                                                                            for (let index = 0; index < filenames.length; index++) {
                                                                                fs.unlink(filenames[index], (error) => {
                                                                                    if (error) {
                                                                                        console.log(error);
                                                                                    }
                                                                                    else {
                                                                                        //console.log("success");
                                                                                    }
                                                                                })
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                 }
            });
        }
    })
}

module.exports = problem2;

