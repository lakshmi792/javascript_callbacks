const fs = require('fs');

const problem1 = (num, callBack) => {
    let number = Math.floor(Math.random() * 10);
    fs.mkdir("callBack", (error) => {
        if (error) {
            console.log(error);
        }
        else {
            for (let index = 0; index < number; index++) {
                fs.writeFile(`${callBack}/file${index}`, '{"country":"India"}', (error) => {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        fs.unlink(`${callBack}/file${index}`, (error) => {
                            if (error) {
                                console.log(error)
                            }
                            else {
                                console.log("success");
                            }
                        })
                    }
                })
            }
        }

    })
}


module.exports = problem1;
